using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework333
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = { 5,4,3,2,11,4,3,2,15,6,7,14,5,18,5,6,8,4,19,4,8,3,5,6,4,5,7,17,5,17,1,2,14,6,19,0,14};
            Console.WriteLine(arr.Length);
            List<int> sortedListOfMarks = new List<int>();
            sortedListOfMarks = MyMath.Sort(arr);
            Print(sortedListOfMarks);
            Console.WriteLine();        
        }

        static void Print(List<int> list)
        {
            foreach (int item in list)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
        }

        static void Print(int[] arr)
        {
            foreach (int item in arr)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
        }
    }
   

    public static class MyMath
    {
        public static bool IsPrime(int value)
        {
            if (value == 2)
                return true;
            if (value < 2 || value % 2 == 0)
                return false;
                for (int i = 3; i < Math.Sqrt(value); i += 2)
                {
                    if (value % i == 0)
                        return false;
                }
            return true;
        }

        public static void Swap(int[] array, int index1, int index2)
        {
            int temp = array[index1];
            array[index1] = array[index2];
            array[index2] = temp;
        }

        public static void Shuffle<T>(T[] arr)
        {
            Random rand = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                T temp = arr[i];
                int r = rand.Next(i, arr.Length);
                arr[i] = arr[r];
                arr[r] = temp;
            }
        }
      public  static List<int> Sort(int[] array)
        {
            int[] arr1 = new int[21];
            for (int i = 0; i < array.Length; i++)
            {
                arr1[array[i]]++;
            }
            List<int> markList = new List<int>();
            for (int i = 0; i < arr1.Length; i++)
            {
                int quantityOfEachNum = arr1[i];
                while (quantityOfEachNum != 0)
                {
                    markList.Add(i);
                    quantityOfEachNum--;
                }
            }
            return markList;
        }

    }

}



