using Microsoft.VisualStudio.TestTools.UnitTesting;
using homework333;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework333.Tests
{
    [TestClass()]
    public class MyMathTests
    {
        [TestMethod()]
        public void SortTest()
        {
            int[] arr = { 1, 14, 5, 13, 2, 16, 8, 5, 3, 4, 6, 7, 11, 14, 3, 15, 3, 11, 12, 3, 1, 4, 5, 11 };
            int length1 = arr.Length;
            List<int> sortedListOfMarks = new List<int>();
            sortedListOfMarks = MyMath.Sort(arr);
            int length2 = sortedListOfMarks.Count;
            bool haveEqualNumberOfElements = false;
            if (length1 == length2)
                haveEqualNumberOfElements = true;
            bool isSorted = true; 
            for(int i = 0; i < length2 - 1; i++)
            {
                if(sortedListOfMarks[i]>sortedListOfMarks[i+1])
                {
                    isSorted = false;
                    break;
                }
            }
            bool finalAnswer = false;
            if (isSorted && haveEqualNumberOfElements)
                finalAnswer = true;
            Assert.IsTrue(finalAnswer);
        }
        [TestMethod()]
        public void IsPrimeTest()
        {
            Assert.IsTrue(MyMath.IsPrime(7));
            Assert.IsTrue(MyMath.IsPrime(3));
            Assert.IsFalse(MyMath.IsPrime(0));
            Assert.IsFalse(MyMath.IsPrime(-2));
            Assert.IsFalse(MyMath.IsPrime(8));
            Assert.IsFalse(MyMath.IsPrime(-5));
            Assert.IsTrue(MyMath.IsPrime(2));
            Assert.IsFalse(MyMath.IsPrime(-7));
            Assert.IsFalse(MyMath.IsPrime(-11));
        }

        [TestMethod()]
        public void SwapTest()
        {
            int[] arr = { 3, 5, -1, 3, 5, -6, 8, 3, -4, 6, 7, 9, 0, 12, -15, 1, 1, 1, 3, 5, 6, 0, -1, -1 - 1, -456 };
            int index1 = 1;
            int index2 = 9;
            int num1 = arr[index1];
            int num2 = arr[index2];
            MyMath.Swap(arr, index1, index2);
            bool isSwaped = false;
            if (num1 == arr[index2] && num2 == arr[index1])
                isSwaped = true;
            Assert.IsTrue(isSwaped);
        }
        
        [TestMethod()]
        public void ShuffleTest()
        {
            int[] arr = { 12, 13, 14, 19, 21, 7, 3, -7, -16, -54 };
            List<int> myList = new List<int>();
            for(int i = 0; i < arr.Length; i++)
            {
                myList.Add(arr[i]);
            }
            MyMath.Shuffle(arr);
            int shuffledNums = 0;
            bool isShuff = false;
            for(int i = 0; i < arr.Length; i++)
            {
                if (arr[i] != myList[i])
                    shuffledNums++;
            }
            if (shuffledNums >=  0.3* arr.Length)
                isShuff = true;
            Assert.IsTrue(isShuff);
        }
    }
}